"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.neoPurchaseOrder = void 0;
var typeorm_1 = require("typeorm");
var neoPurchaseOrder = /** @class */ (function () {
    function neoPurchaseOrder() {
    }
    __decorate([
        typeorm_1.ObjectIdColumn(),
        __metadata("design:type", typeorm_1.ObjectID)
    ], neoPurchaseOrder.prototype, "id", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], neoPurchaseOrder.prototype, "orderRefrence", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], neoPurchaseOrder.prototype, "orderCode", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], neoPurchaseOrder.prototype, "status", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], neoPurchaseOrder.prototype, "category", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], neoPurchaseOrder.prototype, "division", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], neoPurchaseOrder.prototype, "supplier", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], neoPurchaseOrder.prototype, "buyer", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], neoPurchaseOrder.prototype, "buyerOrderNo", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], neoPurchaseOrder.prototype, "buyerRef", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], neoPurchaseOrder.prototype, "agent", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], neoPurchaseOrder.prototype, "quantity", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], neoPurchaseOrder.prototype, "totalValue", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], neoPurchaseOrder.prototype, "deliveryStartDate", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], neoPurchaseOrder.prototype, "deliveryEndDate", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], neoPurchaseOrder.prototype, "paymentTerm", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], neoPurchaseOrder.prototype, "shipmentTerm", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], neoPurchaseOrder.prototype, "deliveryTerm", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], neoPurchaseOrder.prototype, "countryOfOrgin", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], neoPurchaseOrder.prototype, "countryofDestination", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], neoPurchaseOrder.prototype, "season", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], neoPurchaseOrder.prototype, "delivery", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], neoPurchaseOrder.prototype, "reasonForRevison", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], neoPurchaseOrder.prototype, "theme", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], neoPurchaseOrder.prototype, "deliveryStatus", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], neoPurchaseOrder.prototype, "factoryName", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], neoPurchaseOrder.prototype, "CreatedOn", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], neoPurchaseOrder.prototype, "revisionDate", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], neoPurchaseOrder.prototype, "revision", void 0);
    __decorate([
        typeorm_1.Column("string", { array: true }),
        __metadata("design:type", Array)
    ], neoPurchaseOrder.prototype, "item", void 0);
    neoPurchaseOrder = __decorate([
        typeorm_1.Entity('neoPurchaseOrder')
    ], neoPurchaseOrder);
    return neoPurchaseOrder;
}());
exports.neoPurchaseOrder = neoPurchaseOrder;
