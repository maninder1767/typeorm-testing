import {Entity, ObjectID, ObjectIdColumn, Column} from "typeorm";

@Entity('neoPurchaseOrder')
export class neoPurchaseOrder {

                                    @ObjectIdColumn()
                                    id: ObjectID;           
                                    @Column()
                                    orderRefrence: string;
                                    @Column()
                                    orderCode:string;
                                    @Column()
                                    status:string;
                                    @Column()
                                    category:string;
                                    @Column()
                                    division:string;
                                    @Column()
                                    supplier:string;
                                    @Column()
                                    buyer:string;
                                    @Column()
                                    buyerOrderNo:string;
                                    @Column()
                                    buyerRef:string;
                                    @Column()
                                    agent:string;
                                    @Column()
                                    quantity:string;
                                    @Column()
                                    totalValue:string;
                                    @Column()
                                    deliveryStartDate:string;
                                    @Column()
                                    deliveryEndDate:string;
                                    @Column()
                                    paymentTerm:string;
                                    @Column()
                                    shipmentTerm:string;
                                    @Column()
                                    deliveryTerm:string;
                                    @Column()
                                    countryOfOrgin:string;
                                    @Column()
                                    countryofDestination:string;
                                    @Column()
                                    season:string;
                                    @Column()
                                    delivery:string;
                                    @Column()
                                    reasonForRevison:string;
                                    @Column()
                                    theme:string;
                                    @Column()
                                    deliveryStatus:string;
                                    @Column()
                                    factoryName:string;
                                    @Column()
                                    CreatedOn:string;
                                    @Column()
                                    revisionDate:string;
                                    @Column()
                                    revision:string;
                                    @Column("string", {array: true})
                                    item: string[];

}