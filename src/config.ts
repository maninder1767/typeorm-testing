import {createConnection, Connection} from "typeorm";
import {neoPurchaseOrder} from './entity/User';

export const connection = createConnection({
    type: "mongodb",
    host: "localhost",
    port: 27017,
    database: "test",
    entities: [neoPurchaseOrder],
    synchronize: true
});


// export const connection = createConnection({
//     type: "mongodb",
//     host: "3.7.36.153",
//     port: 27017,
//     database: "NNC",
//     username: "admin",
//     password: "wfxneo112233",
//     authSource: "admin",
//     entities: [neoPurchaseOrder],
//     synchronize: true
// });
