"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var bodyParser = require("body-parser");
var User_1 = require("./entity/User");
var config_1 = require("./config");
// create typeorm connection
config_1.connection.then(function (connection) {
    var userRepository = connection.getRepository(User_1.neoPurchaseOrder);
    var poCollection = connection.getMongoRepository(User_1.neoPurchaseOrder);
    // create and setup express app
    var app = express();
    app.use(bodyParser.json());
    // register routes
    app.get("/getAllPO", function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var users;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, poCollection.find()];
                    case 1:
                        users = _a.sent();
                        res.json(users);
                        return [2 /*return*/];
                }
            });
        });
    });
    app.get("/getPOByID/:id", function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var results;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, userRepository.findOne(req.params.id)];
                    case 1:
                        results = _a.sent();
                        return [2 /*return*/, res.send(results)];
                }
            });
        });
    });
    // app.get("/poData", async function(req: Request, res: Response) {   
    //     const users = await data.aggregate([{$project: { _id: 0}}]);
    //     console.log(users);
    //     res.send(users);
    // });
    app.post("/insertPO", function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var results;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(req.body);
                        return [4 /*yield*/, poCollection.insertOne(req.body)];
                    case 1:
                        results = _a.sent();
                        res.send("data inserted" + results);
                        return [2 /*return*/];
                }
            });
        });
    });
    app.put("/updatePO/:id", function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var user;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(req.params.id);
                        return [4 /*yield*/, poCollection.findOneAndUpdate({ "ordeReference": req.params.id }, { $set: { "revision": "3" } }, { returnOriginal: false }).then(function (data) {
                                if (data == null) {
                                    console.log("data not updated");
                                }
                                res.json(data);
                            })];
                    case 1:
                        user = _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    });
    app.post("/deletePO/:id", function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var results;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, poCollection.deleteOne({ "ordeReference": req.params.id })];
                    case 1:
                        results = _a.sent();
                        return [2 /*return*/, res.send("PO has been deleted" + results)];
                }
            });
        });
    });
    // app.get("/deletePO/:id", async function(req: Request, res: Response) {
    //     try{
    //         await poCollection.deleteMany({ "_id": req.params.id}).then((result) => {
    //             console.log("record has been deleted" + result);
    //         });
    //         res.send("PO has been deleted");
    //     } catch (e) {
    //         res.send(e);
    //     }      
    // });
    // start express server
    app.listen(3000);
});
