"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.connection = void 0;
var typeorm_1 = require("typeorm");
var User_1 = require("./entity/User");
exports.connection = typeorm_1.createConnection({
    type: "mongodb",
    host: "localhost",
    port: 27017,
    database: "test",
    entities: [User_1.neoPurchaseOrder],
    synchronize: true
});
// export const connection = createConnection({
//     type: "mongodb",
//     host: "3.7.36.153",
//     port: 27017,
//     database: "NNC",
//     username: "admin",
//     password: "wfxneo112233",
//     authSource: "admin",
//     entities: [neoPurchaseOrder],
//     synchronize: true
// });
