import * as express from "express";
import {Request, Response} from "express";
import * as bodyParser from  "body-parser";
import {neoPurchaseOrder} from "./entity/User";
import {connection} from "./config";
// create typeorm connection
connection.then(connection => {
    const userRepository = connection.getRepository(neoPurchaseOrder);
    const poCollection = connection.getMongoRepository(neoPurchaseOrder);
    // create and setup express app
    const app = express();
    app.use(bodyParser.json());

    // register routes

    app.get("/getAllPO", async function(req: Request, res: Response) {
        const users = await poCollection.find();
        res.json(users);
    });

    app.get("/getPOByID/:id", async function(req: Request, res: Response) {
        const results = await userRepository.findOne(req.params.id);
        return res.send(results);
    });

    // app.get("/poData", async function(req: Request, res: Response) {   
    //     const users = await data.aggregate([{$project: { _id: 0}}]);
    //     console.log(users);
    //     res.send(users);
    // });

    app.post("/insertPO", async function(req: Request, res: Response) {
        console.log(req.body);
        const results = await poCollection.insertOne(req.body);
        res.send("data inserted" + results);
    });

    app.put("/updatePO/:id", async function(req: Request, res: Response) {
        console.log(req.params.id);
        const user = await poCollection.findOneAndUpdate({"ordeReference": req.params.id}, {$set: { "revision": "3"}}, {returnOriginal: false}).then((data) => {
            if(data == null) {
                console.log("data not updated");
            }
            res.json(data);
        });
        // userRepository.merge(user, req.body);
        // const results = await userRepository.save(user);
        // return res.send(user);
    });

    app.post("/deletePO/:id", async function(req: Request, res: Response) {
        const results = await poCollection.deleteOne({"ordeReference": req.params.id});
        return res.send("PO has been deleted" + results);
    });
    // app.get("/deletePO/:id", async function(req: Request, res: Response) {
    //     try{
    //         await poCollection.deleteMany({ "_id": req.params.id}).then((result) => {
    //             console.log("record has been deleted" + result);
    //         });
    //         res.send("PO has been deleted");
    //     } catch (e) {
    //         res.send(e);
    //     }      
    // });
    // start express server
    app.listen(3000);
});